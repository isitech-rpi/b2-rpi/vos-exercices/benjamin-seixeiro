#include <Windows.h>
#include <iostream>
#include <iomanip>
#include <time.h>
using namespace std;

#undef max

//1.1
void declaration() {
	int a = 1;
	int b = 5;
	double f = 3.14;
	char c('C');
	char d;
	cout << "a = " << a << "\nadress = " << hex << &a << endl << endl;
	cout << "b = " << b << "\nadress = " << hex << &b << endl << endl;
	cout << "f = " << f << "\nadress = " << hex << &f << endl << endl;
	cout << "c = " << c << "\nadress = " << hex << &c << endl << endl;
	cout << "d = " << d << "\nadress = " << hex << &d << endl << endl;

	d = 'D';
	cout << "d = " << d << "\nadress = " << hex << &d << endl << endl;

	decltype(a) temp = a;
	a = b;
	b = temp;
	cout << "a = " << a << "\nadress = " << hex << &a << endl << endl;
	cout << "b = " << b << "\nadress = " << hex << &b << endl << endl;

	temp = a;
	a = (int)f;
	f = temp;
	cout << "a = " << a << "\nadress = " << hex << &a << endl << endl;
	cout << "f = " << f << "\nadress = " << hex << &f << endl << endl;

	a = (int)f;
	cout << "a = " << a << "\nadress = " << hex << &a << endl << endl;
	cout << "f = " << f << "\nadress = " << hex << &f << endl << endl;

	d = 90;
	cout << "d = " << d << "\nnumeric value = " << static_cast<int>(d) << endl << endl;

	d += 255;
	cout << "d = " << d << "\nnumeric value = " << static_cast<int>(d) << endl << endl;

}

//1.2
void calcul() {
	int i1 = 1;
	int i2 = 3;
	int r1;
	double d1 = 1;
	double d2 = 3;
	double r2;

	r1 = (i1 / i2);
	cout << "r1 = " << r1 << endl << endl;

	r2 = (i1 / i2);
	cout << "r2 = (i1 / i2)\n" << "r2 = " << r2 << endl << endl;

	r2 = (d1 / i2);
	cout << "r2 = (d1 / i2)\n" << "r2 = " << r2 << endl << endl;

	r2 = (i1 / d2);
	cout << "r2 = (i1 / d2)\n" << "r2 = " << r2 << endl << endl;

	r2 = (d1 / d2);
	cout << "r2 = (d1 / d2)\n" << "r2 = " << r2 << endl << endl;
}

//1.3
void pointeur() {
	int a = 3;
	int b = 10;
	int* p;

	p = &b;
	cout << "Pointeur : " << p << "; Adresse : " << &p << "; Valeur Pointe : " << dec << *p << endl;

	p = &a;
	*p = *p * 2;
	p += 1;
	cout << "Pointeur : " << p << "; Adresse : " << &p << "; Valeur Pointe : " << dec << *p << endl;
}

//2.1 Horloge
void horloge() {
	int heure, minute;

	cout << "Saisissez l'heure : ";
	cin >> heure;
	while (std::cin.fail() || heure < 0 || heure > 23)
	{
		cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		cout << endl << " Saisie incorrect, veuillez saisir un nombre correct (0-23): ";
		cin >> heure;
	}

	cout << "Saisissez les minutes : ";
	cin >> minute;
	while (std::cin.fail() || minute < 0 || minute > 59)
	{
		cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		cout << endl << " Saisie incorrect, veuillez saisir un nombre correct (0-59): ";
		cin >> minute;
	}

	cout << endl << "Vous avez saisi l'heure : " << std::setfill('0') << std::setw(2) << heure << ":" << std::setfill('0') << std::setw(2) << minute << endl;

	minute++;
	if (minute == 60) {
		minute = 0;
		heure++;
		if (heure == 24) {
			heure = 0;
		}
	}
	cout << "Dans un futur proche il sera : " << std::setfill('0') << std::setw(2) << heure << ":" << std::setfill('0') << std::setw(2) << minute;

	cout << endl << endl << endl;
}

//2.2
int input_age() {
	int age;
	cout << "Il s’agit de déterminer votre catégorie sportive." << endl << endl;
	cout << "Saisissez votre age : ";
	cin >> age;
	while (std::cin.fail() || age < 6 || age > 123)
	{
		cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		if (age > 123) {
			cout << "Etes vous humain ???" << endl;
		}
		cout << endl << " Saisie incorrect, veuillez saisir un nombre correct (6-123): ";
		cin >> age;
	}
	return age;
}

void categorie_age_if() {
	int age = input_age();

	if (age < 10) { cout << "Vous etes un pre-poussin !" << endl; }
	else if (age < 12) { cout << "Vous etes un poussin !" << endl; }
	else if (age < 14) { cout << "Vous etes un benjamin !" << endl;	}
	else if (age < 16) { cout << "Vous etes un minime !" << endl; }
	else if (age < 18) { cout << "Vous etes un cadet !" << endl;	}
	else { cout << "Vous etes un senior !" << endl; }
}

void categorie_age_switch() {
	int age = input_age();

	switch (age) {
	case 6:
	case 7:
	case 8:
	case 9:
		cout << "Vous etes un pre-poussin !" << endl;
		break;
	case 10:
	case 11:
		cout << "Vous etes un poussin !" << endl;
		break;
	case 12:
	case 13:
		cout << "Vous etes un benjamin !" << endl;
		break;
	case 14:
	case 15:
		cout << "Vous etes un minime !" << endl;
		break;
	case 16:
	case 17:
		cout << "Vous etes un cadet !" << endl;
		break;
	default:
		cout << "Vous etes un senior !" << endl;
		break;
	}
}

//2.3
void notes() {
	double note, somme = 0;
	int compteur = 0;

	cout << "Entrez vos notes (tapez -1 pour terminer) : " << endl;
	while (true) {
		cout << "Note " << compteur + 1 << " : ";
		cin >> note;

		if (note == -1) {
			break;
		}

		if (note >= 0 && note <= 20) {
			somme += note;
			compteur++;
		}
		else {
			cout << "Note invalide, veuillez entrer une note entre 0 et 20." << endl;
		}
	}

	if (compteur > 0) {
		double moyenne = somme / compteur;
		cout << "La moyenne des notes est : " << moyenne << endl;
	}
	else {
		cout << "Aucune note valide n'a été saisie." << endl;
	}
}

//3.1
int SaisieNombre(int mini, int maxi) {
	int unEntier;

	cout << "Entrez un entier entre " << mini << " et " << maxi << " : ";
	cin >> unEntier;
	while (std::cin.fail() || unEntier < mini || unEntier > maxi)
	{
		cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		cout << endl << " Saisie incorrect, veuillez saisir un nombre correct (entre " << mini << " et " << maxi << ") : ";
		cin >> unEntier;
	}

	return unEntier;
}

//3.2
string TirerUneCarte(string gameCard) {
	srand(time(NULL));
	string colors[4] = {
		"Pique", "Carreau", "Trefle", "Coeur"
	};
	string cards32[8] = {
		"AS", "Roi", "Dame", "Valet", "10", "9", "8", "7"
	};
	string cards52[13]{
		"AS", "Roi", "Dame", "Valet", "10", "9", "8", "7", "6", "5", "4", "3", "2"
	};
	string tarot[22]{
		"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "Excuse"
	};
	string* pointeur = cards32;
	int gameCardsSize = 8;

	string color;
	int random_color = rand() % 4;
	if (gameCard == "Tarot") {
		pointeur = rand() % 5 == 4 ? tarot : cards52;
		color = "Tarot";
		gameCardsSize = 22;
	}
	else if (gameCard == "52"){
		pointeur = cards52;
		gameCardsSize = 13;
		color = colors[random_color];
	}
	
	int random_card = rand() % gameCardsSize;
	string card = pointeur[random_card];

	return card + "_" + color;
}

//3.3
void permuter(int* a, int* b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

//4.1
void remplirTab(int* tab) {
	cout << "Entrez des nombres. Tapez -1 pour arreter: " << endl;
	do {
		int nombre;
		cin >> nombre;
		while (cin.fail()) {
			cout << "Saisie incorrect, veuillez saisir un nombre correct : ";
			cin >> nombre;
		}

		if (nombre == -1) {
			break;
		}

		*tab = nombre;
		tab++;

	} while (true);
}

//4.2
int* copie(const int* src, int lngTab) {
	int* newTab = new int[lngTab];
	for (int i = 0; i < lngTab; i++) {
		newTab[i] = src[i];
	}
	return newTab;
}

//4.3
void sortTab(int* tab1, int lngTab, bool ordreCroissant = true) {
	for (int i = 0; i < lngTab - 1; i++) {
		int indExt = i;
		for (int j = i + 1; j < lngTab; j++) {
			if (ordreCroissant ? tab1[j] < tab1[indExt] : tab1[j] > tab1[indExt]) {
				indExt = j;
			}
		}
		if (indExt != i) {
			int temp = tab1[i];
			tab1[i] = tab1[indExt];
			tab1[indExt] = temp;
		}
	}
}


int main() 
{
	SetConsoleOutputCP(CP_UTF8);
	// 1 Les Variable
	// 1.1 Déclaration, affectation, affichage
	//declaration();

	//1.2 Opération sur les nombres
	//calcul();

	//1.3 Pointeur
	//pointeur();

	// 2 Conditions et Iterations
	//2.1 Conditions Simples
	//horloge();

	//2.2 Conditions Multiples
	//categorie_age_if();
	//categorie_age_switch();

	//2.3 Iterations
	//notes();

	// 3 Fonctions
	// 3.1 Saisie d'une nombre
	//cout << SaisieNombre(12, 45);

	//3.2 Tirer une carte
	//cout << TirerUneCarte("");

	// 3.3 Permuter
	//int a = 3;
	//int b = 5;
	//permuter(&a, &b);
	//cout << a << " / " << b;

	// 4 Tableaux
	// 4.1 Remplir
	int taille = 5;
	int* p = new int[taille];
	remplirTab(p);
	delete[] p;

	// 4.2 Copie
	int* copiage = copie(p, taille);

	// 4.3 Tri
	sortTab(copiage, taille);
}
