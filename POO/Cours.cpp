#include "Cours.h"
#include <string>
#include <iostream>
using namespace std;

Cours::Cours() {
    matiere = "C++";
    promo = "B2 RPI";
    cout << "Cr�ation de " << this << " - Mati�re : " << matiere << ", Promo : " << promo << endl;
}

Cours::Cours(string matiere, string promo) {
    this->matiere = matiere;
    this->promo = promo;
    cout << "Cr�ation de " << this << " - Mati�re : " << matiere << ", Promo : " << promo << endl;
}

Cours::~Cours() {
    cout << "Destruction de " << this << " - Mati�re : " << matiere << ", Promo : " << promo << endl;
};

void Cours::setPromo(string promo) {
    this->promo = promo;
}

void Cours::Enseigner() const {
    cout << "Cours " << matiere << " avec la classe " << promo << endl;
}