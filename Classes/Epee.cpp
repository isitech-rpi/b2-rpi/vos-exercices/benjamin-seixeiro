#include "Epee.h"

Epee::Epee() : Armes()
{
}

Epee::Epee(double attaque, double defense, int portee) : Armes(attaque, defense, portee)
{
}

Epee::~Epee()
{
}

double Epee::Utilise(bool attaque)
{
	cout << "Excalibuuuuuuur !!" << endl;
	return attaque ? puissanceAttaque : puissanceDefense;
}
