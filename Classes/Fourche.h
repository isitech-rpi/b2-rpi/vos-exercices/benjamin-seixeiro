#pragma once
#include "Armes.h"
class Fourche : public Armes
{
public : 
	Fourche();
	Fourche(double attaque, double defense, int portee);
	~Fourche();

	double Utilise(bool attaque);
};

