#include "Personnages.h"

bool Personnages::Defendre(Personnages* attaquant)
{
	if (this->EstVivant()) {
		double degatsArmes = 0.0;
		if (this->armeCourante != nullptr) {
			degatsArmes = this->armeCourante->Utilise(false);
		}
		attaquant->vie -= ( this->vie + degatsArmes) * this->niveau / 20;
	}
	return this->EstVivant();
}

Personnages::Personnages(string nom)
{
	this->nom = nom;
	this->armeCourante = nullptr;
	cout << "Cr�ation de M." << nom << endl;
}

Personnages::~Personnages()
{
	cout << "Delete de M." << nom << endl;
}

bool Personnages::Attaquer(Personnages* ennemi)
{
	if (ennemi->EstVivant()) {
		double degatsArmes = 0.0;
		if (this->armeCourante != nullptr) {
			degatsArmes = this->armeCourante->Utilise(true);
		}
		ennemi->vie -= (this->vie + degatsArmes) * this->niveau / 20;
		ennemi->Defendre(this);
	}
	return this->EstVivant();
}

bool Personnages::EstVivant()
{
	return this->vie > 0;
}

void Personnages::CollecterArme(Armes* arme)
{
	cout << nom << " � trouver une arme !" << endl;
	this->armeCourante = arme;
}


