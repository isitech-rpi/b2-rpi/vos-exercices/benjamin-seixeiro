#include <iostream>
#include <Windows.h>
#include "Heros.h";
#include "Fourche.h";
#include "Epee.h";

ostream& operator<<(ostream& os, const Personnages& perso)
{
    if (perso.GetVie() > 0) {
        os << "Je suis " << perso.GetNom() << ", de niveau " << perso.GetNiveau() << " et il me reste " << perso.GetVie() << " hp." << endl;
    }
    else {
        os << "Dans la jungle, terrible jungle, " << perso.GetNom() << " est mort ce soir." << endl;
    }
    return os;
}

int main()
{
    SetConsoleOutputCP(CP_UTF8);

    Personnages guethenoc("Guethenoc");
    Personnages * balmeg = new Personnages("Balcmeg");
    Heros goulum("Goulum");
    Heros* frodon = new Heros("Frodon", 10);
    
    cout << guethenoc;
    cout << *balmeg;
    cout << goulum;
    cout << *frodon << endl;

    guethenoc.Attaquer(balmeg);
    frodon->Attaquer(&goulum);

    cout << guethenoc;
    cout << *balmeg;
    cout << goulum;
    cout << *frodon << endl;

    Fourche armes1(4, 1, 3);
    Armes* armes2 = new Epee(2, 3, 4);

    guethenoc.CollecterArme(&armes1);
    goulum.CollecterArme(&armes1);
    frodon->CollecterArme(armes2);

    guethenoc.Attaquer(balmeg);
    frodon->Attaquer(&goulum);

    cout << guethenoc;
    cout << *balmeg;
    cout << goulum;
    cout << *frodon << endl;

    delete frodon;
    delete balmeg;
    delete armes2;
}

