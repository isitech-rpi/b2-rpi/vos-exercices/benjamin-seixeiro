#include "Fourche.h"

Fourche::Fourche() : Armes()
{
}

Fourche::Fourche(double attaque, double defense, int portee) : Armes(attaque, defense, portee)
{
}

Fourche::~Fourche()
{
}

double Fourche::Utilise(bool attaque)
{
	cout << "Prends �a et enfourche les tous !" << endl;
	return attaque ? puissanceAttaque : puissanceDefense;
}
