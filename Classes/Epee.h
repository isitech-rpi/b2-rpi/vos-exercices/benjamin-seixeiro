#pragma once
#include "Armes.h";

class Epee : public Armes
{
public:
	Epee();
	Epee(double attaque, double defense, int portee);
	~Epee();

	double Utilise(bool attaque);
};

