#pragma once
#include <iostream>
#include "Armes.h"
using namespace std;

class Personnages
{
	private :
		string nom;
	
	protected :
		double vie = 10.0;
		int niveau = 1;
		Armes* armeCourante;
		
		bool Defendre(Personnages* attaquant);

	public:
		Personnages(string nom);
		~Personnages();

		string GetNom() { return this->nom; }
		string GetNom() const { return this->nom; }
		int GetNiveau() { return this->niveau; }
		int GetNiveau() const { return this->niveau; }
		double GetVie() { return this->vie; }
		double GetVie() const { return this->vie; }		

		bool Attaquer(Personnages* ennemi);
		bool EstVivant();
		void CollecterArme(Armes* arme);
};

