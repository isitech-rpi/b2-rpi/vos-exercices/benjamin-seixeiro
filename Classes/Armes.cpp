#include "Armes.h"

Armes::Armes()
{
	cout << "Cr�ation d'une Armes vide" << endl;
}

Armes::Armes(double attaque, double defense, int portee)
{
	this->puissanceAttaque = attaque;
	this->puissanceDefense = defense;
	this->portee = portee;

	cout << "Cr�ation d'une Armes Initialis�" << endl;
}

Armes::~Armes()
{
	cout << "Destruction d'une Armes" << endl;
}

double Armes::Utilise(bool attaque)
{
	cout << "Attaque : " << puissanceAttaque << " Defense : " << puissanceDefense << " Portee : " << portee << endl;
	return attaque ? puissanceAttaque : puissanceDefense;
}
