#pragma once
#include <iostream>
using namespace std;

class Armes
{
	protected:
		double puissanceAttaque = 0.0;
		double puissanceDefense = 0.0;
		int portee = 0;

	public:
		Armes();
		Armes(double attaque, double defense, int portee);
		~Armes();

		virtual double Utilise(bool attaque) = 0;

};

